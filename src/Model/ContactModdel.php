<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ContactModdel extends AbstractModel
{
    protected static $table = 'contact';

    protected $id;
    protected $sujet;
    protected $email;
    protected $content;
    protected $created_at;
}